"use client";

import React, { useEffect, useState } from 'react';
import Navbar from '@/components/navbar';
import { FaEdit, FaTrashAlt } from 'react-icons/fa';
import axios from '@/utils/axiosConfig';
import { useForm } from 'react-hook-form';

interface Product {
    id: number;
    name: string;
    description: string;
    brand: string;
    category: string;
    type: string;
    subtype: string;
    country: string;
}

const ProductListing: React.FC = () => {
    const [products, setProducts] = useState<Product[]>([]);
    const { register, handleSubmit, setValue } = useForm();

    useEffect(() => {
        // Requisição dos dados dos produtos
        axios.get('/products')
            .then(response => {
                setProducts(response.data);
            })
            .catch(error => {
                console.error('Erro ao solicitar os dados do produto:', error);
            });
    }, []);

    const handleEdit = (product: Product) => {
        setValue('name', product.name);
        setValue('description', product.description);
        setValue('brand', product.brand);
        setValue('category', product.category);
        setValue('type', product.type);
        setValue('subtype', product.subtype);
        setValue('country', product.country);
    };

    const handleDelete = (id: number) => {
        //Deleta os dados da api
        axios.delete(`/products/${id}`)
            .then(response => {
                setProducts(products.filter(product => product.id !== id));
            })
            .catch(error => {
                console.error('Erro ao deletar o produto:', error);
            });
    };

    return (
        <div className='flex flex-row'>
            <Navbar />
            <div className="flex flex-col items-center justify-start min-h-screen h-full w-full px-4">
                <h1 className="text-2xl text-primary p-4 mb-6 font-semibold">Listagem de Produtos</h1>
                <table className="table-auto mb-4 bg-white shadow-lg w-full">
                    <thead className="text-sm text-center text-white bg-primary p-2">
                        <tr>
                            <th className="p-3">Cód.</th>
                            <th className="p-3">Nome</th>
                            <th className="p-3">Descrição</th>
                            <th className="p-3">Marca</th>
                            <th className="p-3">Categoria</th>
                            <th className="p-3">Tipo</th>
                            <th className="p-3">SubTipo</th>
                            <th className="p-3">País/Origem</th>
                            <th className="p-3">Ações</th>
                        </tr>
                    </thead>
                    <tbody className="text-sm font-semibold text-center">
                        {products.map(product => (
                            <tr key={product.id} className="cursor-pointer">
                                <td className="p-3">{product.id}</td>
                                <td className="p-3">{product.name}</td>
                                <td className="p-3">{product.description}</td>
                                <td className="p-3">{product.brand}</td>
                                <td className="p-3">{product.category}</td>
                                <td className="p-3">{product.type}</td>
                                <td className="p-3">{product.subtype}</td>
                                <td className="p-3">{product.country}</td>
                                <td className="p-3 flex justify-center space-x-2">
                                    <button onClick={() => handleEdit(product)} className="text-yellow-500 hover:text-yellow-700">
                                        <FaEdit size={18} />
                                    </button>
                                    <button onClick={() => handleDelete(product.id)} className="text-red-500 hover:text-red-700">
                                        <FaTrashAlt size={18} />
                                    </button>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    );
};

export default ProductListing;
