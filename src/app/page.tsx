import ProductListing from "./productListing/productListing";


export default function Page() {
  return (
    <main className="min-h-screen h-full items-center justify-center bg-slate-100">
          <ProductListing />
    </main>
  );
}
