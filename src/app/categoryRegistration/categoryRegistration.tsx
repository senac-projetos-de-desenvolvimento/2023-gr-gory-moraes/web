"use client";

import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import Navbar from '@/components/navbar';
import ButtonRegistration from '@/components/buttonRegistration';
import axios from '@/utils/axiosConfig';

interface FormData {
    categoryName: string;
    type: string;
    subtype: string;
}

const CategoryRegistration: React.FC = () => {
    const { register, handleSubmit, reset, formState: { errors } } = useForm<FormData>();
    const [types, setTypes] = useState<string[]>([]);
    const [subtypes, setSubtypes] = useState<string[]>([]);
    
    useEffect(() => {
        // Requisição que pega os dados dos tipos da api
        axios.get('/types')
            .then(response => {
                setTypes(response.data);
            })
            .catch(error => {
                console.error('Erro ao buscar tipos:', error);
            });
        
        // Requisição que pega os dados dos subtipos da api
        axios.get('/subtypes')
            .then(response => {
                setSubtypes(response.data);
            })
            .catch(error => {
                console.error('Erro ao buscar subtipos:', error);
            });
    }, []);

    const onSubmit = async (data: FormData) => {
        try {
            const response = await axios.post('/categories', data);
            console.log('Categoria cadastrada com sucesso:', response.data);
            reset(); // Reseta o formulário após o envio bem-sucedido
        } catch (error) {
            console.error('Erro ao cadastrar categoria:', error);
        }
    };

    return (
        <div className="flex h-screen">
            <Navbar />
            <div className="flex-1 items-center px-4 overflow-auto bg-gray-100">
                <div className="flex flex-col items-center justify-center min-h-screen">
                    <h1 className="text-2xl text-center p-2 mb-4 font-bold">Cadastro da Categoria</h1>
                    <form onSubmit={handleSubmit(onSubmit)} className="bg-white p-6 shadow-lg rounded-lg w-full max-w-3xl">
                        <div className="mb-2 p-2 text-sm">
                            <label htmlFor="categoryName" className="block font-semibold mb-2">Nome da Categoria</label>
                            <input
                                type="text"
                                id="categoryName"
                                {...register('categoryName', { required: 'Este campo é obrigatório' })}
                                placeholder='Digite nome da categoria'
                                className="w-full px-3 py-2 border border-gray-300 rounded-sm focus:outline-none focus:ring-2 focus:ring-red-950"
                            />
                            {errors.categoryName && <p className="text-red-500 text-xs mt-1 ml-1">{errors.categoryName.message}</p>}
                        </div>

                        <div className="flex flex-row text-sm">
                            <div className="md:w-1/2 mb-2 p-2">
                                <label htmlFor="type" className="block font-semibold mb-2">Tipo</label>
                                <select
                                    id="type"
                                    {...register('type', { required: 'Este campo é obrigatório' })}
                                    className="w-full px-3 py-2 border border-gray-300 rounded-sm focus:outline-none focus:ring-2 focus:ring-red-950"
                                >
                                    <option value="">Selecione um tipo</option>
                                    {types.map((type, index) => (
                                        <option key={index} value={type}>{type}</option>
                                    ))}
                                </select>
                                {errors.type && <p className="text-red-500 text-xs mt-1 ml-1">{errors.type.message}</p>}
                            </div>
                            <div className="text-sm md:w-1/2 mb-2 p-2">
                                <label htmlFor="subtype" className="block font-semibold mb-2">SubTipo</label>
                                <select
                                    id="subtype"
                                    {...register('subtype', { required: 'Este campo é obrigatório' })}
                                    className="w-full px-3 py-2 border border-gray-300 rounded-sm focus:outline-none focus:ring-2 focus:ring-red-950"
                                >
                                    <option value="">Selecione um subtipo</option>
                                    {subtypes.map((subtype, index) => (
                                        <option key={index} value={subtype}>{subtype}</option>
                                    ))}
                                </select>
                                {errors.subtype && <p className="text-red-500 text-xs mt-1 ml-1">{errors.subtype.message}</p>}
                            </div>
                        </div>

                        <div className="flex items-end justify-end p-2">
                            <ButtonRegistration />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default CategoryRegistration;
