"use client";

import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import axios from '@/utils/axiosConfig';
import Navbar from '@/components/navbar';
import ButtonRegistration from '@/components/buttonRegistration';

interface FormData {
    productName: string;
    description: string;
    photo: string;
    brand: string;
    category: string;
    type: string;
    subtype: string;
    country: string;
}

const ProductRegistration: React.FC = () => {
    const { register, handleSubmit, reset, formState: { errors } } = useForm<FormData>();
    const [brands, setBrands] = useState<string[]>([]);
    const [categories, setCategories] = useState<string[]>([]);
    const [types, setTypes] = useState<string[]>([]);
    const [subtypes, setSubtypes] = useState<string[]>([]);
    const [countries, setCountries] = useState<string[]>([]);

    useEffect(() => {
        // Requisição que pega os dados(marca, categoria, tipo, subtipo e países) do banco
        const fetchData = async () => {
            try {
                const [brandsRes, categoriesRes, typesRes, subtypesRes, countriesRes] = await Promise.all([
                    axios.get('/brands'),
                    axios.get('/categories'),
                    axios.get('/types'),
                    axios.get('/subtypes'),
                    axios.get('/countries')
                ]);
                setBrands(brandsRes.data);
                setCategories(categoriesRes.data);
                setTypes(typesRes.data);
                setSubtypes(subtypesRes.data);
                setCountries(countriesRes.data);
            } catch (error) {
                console.error('Erro ao buscar dados:', error);
            }
        };

        fetchData();
    }, []);

    const onSubmit = async (data: FormData) => {
        try {
            const response = await axios.post('/products', data);
            console.log('Produto cadastrada com sucesso:', response.data);
            reset(); // Reseta o formulário após o envio bem-sucedido
        } catch (error) {
            console.error('Erro ao cadastrar produto:', error);
        }
    };

    return (
        <div className="flex h-screen">
            <Navbar />
            <div className="flex-1 items-center px-4 overflow-auto bg-gray-100">
                <div className="flex flex-col items-center justify-center min-h-screen">
                    <h1 className="text-2xl text-center p-2 mb-4 font-bold">Cadastro de Produto</h1>
                    <form onSubmit={handleSubmit(onSubmit)} className="bg-white p-6 shadow-lg rounded-lg w-full max-w-3xl">
                        <div className="mb-2 p-2 text-sm">
                            <label htmlFor="name" className="block font-semibold mb-2">Nome do Produto</label>
                            <input
                                type="text"
                                id="name"
                                {...register('productName', { required: 'Este campo é obrigatório' })}
                                className="w-full px-3 py-2 border border-gray-300 rounded-sm focus:outline-none focus:ring-2 focus:ring-red-950"
                                placeholder='Digite o nome do produto'
                            />
                            {errors.productName && <p className="text-red-500 text-xs mt-1 ml-1">{errors.productName.message}</p>}
                        </div>

                        <div className="flex flex-row text-sm">
                            <div className="md:w-1/2 mb-2 p-2">
                                <label htmlFor="description" className="block font-semibold mb-2">Descrição</label>
                                <textarea
                                    id="description"
                                    {...register('description', { required: 'Este campo é obrigatório' })}
                                    className="w-full px-3 py-2 border border-gray-300 rounded-sm focus:outline-none focus:ring-2 focus:ring-red-950"
                                    placeholder='Digite uma descrição do produto'
                                />
                                {errors.description && <p className="text-red-500 text-xs mt-1 ml-1">{errors.description.message}</p>}
                            </div>

                            <div className="md:w-1/2 mb-2 p-2">
                                <label htmlFor="photo" className="block font-semibold mb-2">Foto</label>
                                <input
                                    type="text"
                                    id="photo"
                                    {...register('photo', { required: 'Este campo é obrigatório' })}
                                    className="w-full px-3 py-2 border border-gray-300 rounded-sm focus:outline-none focus:ring-2 focus:ring-red-950"
                                />
                                {errors.photo && <p className="text-red-500 text-xs mt-1 ml-1">{errors.photo.message}</p>}
                            </div>
                        </div>

                        <div className="flex flex-row text-sm">
                            <div className="md:w-1/2 mb-2 p-2">
                                <label htmlFor="brand" className="block font-semibold mb-2">Marca</label>
                                <select
                                    id="brand"
                                    {...register('brand', { required: 'Este campo é obrigatório' })}
                                    className="w-full px-3 py-2 border border-gray-300 rounded-sm focus:outline-none focus:ring-2 focus:ring-red-950"
                                >
                                    <option value="">Selecione uma marca</option>
                                    {brands.map((brand, index) => (
                                        <option key={index} value={brand}>{brand}</option>
                                    ))}
                                </select>
                                {errors.brand && <p className="text-red-500 text-xs mt-1 ml-1">{errors.brand.message}</p>}
                            </div>

                            <div className="md:w-1/2 mb-2 p-2">
                                <label htmlFor="category" className="block font-semibold mb-2">Categoria</label>
                                <select
                                    id="category"
                                    {...register('category', { required: 'Este campo é obrigatório' })}
                                    className="w-full px-3 py-2 border border-gray-300 rounded-sm focus:outline-none focus:ring-2 focus:ring-red-950"
                                >
                                    <option value="">Selecione uma categoria</option>
                                    {categories.map((category, index) => (
                                        <option key={index} value={category}>{category}</option>
                                    ))}
                                </select>
                                {errors.category && <p className="text-red-500 text-xs mt-1 ml-1">{errors.category.message}</p>}
                            </div>
                        </div>

                        <div className="flex flex-row text-sm">
                            <div className="md:w-1/2 mb-2 p-2">
                                <label htmlFor="type" className="block font-semibold mb-2">Tipo</label>
                                <select
                                    id="type"
                                    {...register('type', { required: 'Este campo é obrigatório' })}
                                    className="w-full px-3 py-2 border border-gray-300 rounded-sm focus:outline-none focus:ring-2 focus:ring-red-950"
                                >
                                    <option value="">Selecione um tipo</option>
                                    {types.map((type, index) => (
                                        <option key={index} value={type}>{type}</option>
                                    ))}
                                </select>
                                {errors.type && <p className="text-red-500 text-xs mt-1 ml-1">{errors.type.message}</p>}
                            </div>
                            <div className="md:w-1/2 mb-2 p-2">
                                <label htmlFor="country" className="block font-semibold mb-2">País/Origem</label>
                                <select
                                    id="country"
                                    {...register('country', { required: 'Este campo é obrigatório' })}
                                    className="w-full px-3 py-2 border border-gray-300 rounded-sm focus:outline-none focus:ring-2 focus:ring-red-950"
                                >
                                    <option value="">Selecione um país</option>
                                    {countries.map((country, index) => (
                                        <option key={index} value={country}>{country}</option>
                                    ))}
                                </select>
                                {errors.country && <p className="text-red-500 text-xs mt-1 ml-1">{errors.country.message}</p>}
                            </div>
                        </div>

                        <div className="flex flex-row">
                            <div className="text-sm md:w-1/2 mb-2 p-2">
                                <label htmlFor="subtype" className="block font-semibold mb-2">SubTipo</label>
                                <select
                                    id="subtype"
                                    {...register('subtype', { required: 'Este campo é obrigatório' })}
                                    className="w-full px-3 py-2 border border-gray-300 rounded-sm focus:outline-none focus:ring-2 focus:ring-red-950"
                                >
                                    <option value="">Selecione um subtipo</option>
                                    {subtypes.map((subtype, index) => (
                                        <option key={index} value={subtype}>{subtype}</option>
                                    ))}
                                </select>
                                {errors.subtype && <p className="text-red-500 text-xs mt-1 ml-1">{errors.subtype.message}</p>}
                            </div>

                            <div className="flex items-end justify-end md:w-1/2 mb-2 p-2">
                                <ButtonRegistration />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default ProductRegistration;
