"use client";

import React from 'react';
import { useForm, SubmitHandler } from 'react-hook-form';
import Image from 'next/image';
import logo from '../../../public/logo2.png';
import background from '../../../public/backgroundLogin.jpg';

interface LoginFormInputs {
    email: string;
    password: string;
}

const Login: React.FC = () => {

    const { register, handleSubmit } = useForm<LoginFormInputs>();


    const handleSignIn: SubmitHandler<LoginFormInputs> = (data) => {
        console.log(data);
    };

    return (
        <div className="flex items-center justify-center min-h-screen h-full">
            <div className="flex flex-col md:flex-row items-center justify-center w-full max-w-screen-lg bg-white shadow-lg rounded-lg overflow-hidden">
                <div className="w-full md:w-1/3 p-6">
                    <Image src={logo} alt="Logo Genovese" className="object-cover items-center w-60 h-auto mx-auto" />
                    <h2 className="text-2xl font-bold  text-primary">Faça seu login</h2>
                    <h4 className="text-xs font-semibold mb-6 text-primary">Entre com suas informações de cadastro.</h4>

                    <form onSubmit={handleSubmit(handleSignIn)}>
                        <div className="mb-4">
                            <label htmlFor="email" className="block text-xs font-semibold mb-2 text-primary">E-mail</label>
                            <input
                                {...register('email')}
                                name='email'
                                type="email"
                                id="email"
                                placeholder='Digite seu e-mail'
                                className="w-full text-sm px-3 py-2 border border-primary rounded-sm focus:outline-none focus:ring-2 focus:ring-red-950"
                                required
                            />
                        </div>
                        <div className="mb-6">
                            <label htmlFor="password" className="block text-xs font-semibold mb-2 text-primary">Password</label>
                            <input
                                {...register('password')}
                                name='password'
                                type="password"
                                id="password"
                                placeholder='Digite sua senha'
                                className="w-full text-sm px-3 py-2 border border-primary rounded-sm focus:outline-none focus:ring-2 focus:ring-red-950"
                                required
                            />
                        </div>
                        <button
                            type="submit"
                            className="w-full bg-primary text-white text-lg font-bold py-2 rounded-sm hover:bg-red-800 transition duration-200"
                        >
                            Login
                        </button>
                    </form>
                </div>
                <div className="hidden md:block md:w-2/3">
                    <Image src={background} alt="imagem de fundo" className="object-cover w-full h-full" style={{ maxHeight: "calc(100vh - 3.5rem)" }} />
                </div>
            </div>
        </div>
    );
};

export default Login;
