"use client";

import React, { useEffect, useState } from 'react';
import { useForm, SubmitHandler } from 'react-hook-form';
import Navbar from '../../components/navbar';
import ButtonRegistration from '@/components/buttonRegistration';
import axiosInstance from '@/utils/axiosConfig';

interface BrandFormInputs {
    brandName: string;
    country: string;
}

const BrandRegistration: React.FC = () => {
    const { register, handleSubmit, reset, formState: { errors } } = useForm<BrandFormInputs>();
    const [countries, setCountries] = useState<string[]>([]);

    useEffect(() => {
        //Requisição dos dados dos países no banco
        axiosInstance.get('/countries')
        .then(response => {
            setCountries(response.data);
        })
        .catch(error => {
            console.error('Erro ao acessar os dados do países:', error);
        });
    }, []);

    const onSubmit: SubmitHandler<BrandFormInputs> = data => {
        console.log(data);
        axiosInstance.post('/brand', data)
            .then(response => {
                console.log('Marca cadastrada com sucesso:', response.data);
                reset();
            })
            .catch(error => {
                console.error('Erro ao cadastrar marca:', error);
            });
    };

    return (
        <div className="flex h-screen">
            <Navbar />
            <div className="flex-1 items-center px-4 overflow-auto bg-gray-100">
                <div className="flex flex-col items-center justify-center min-h-screen">
                    <h1 className="text-2xl text-center p-2 mb-4 font-bold">Cadastro de Marca</h1>
                    <form onSubmit={handleSubmit(onSubmit)} className="bg-white p-6 shadow-lg rounded-lg w-full max-w-3xl">
                        <div className="flex flex-row text-sm">
                            <div className="md:w-1/2 mb-2 p-2">
                                <label htmlFor="brandName" className="block font-semibold mb-2">Nome da Marca</label>
                                <input
                                    type="text"
                                    id="brandName"
                                    placeholder='Digite nome da marca'
                                    className="w-full px-3 py-2 border border-gray-300 rounded-sm focus:outline-none focus:ring-2 focus:ring-red-950"
                                    {...register('brandName', { required: true })}
                                />
                                {errors.brandName && <p className="text-red-600 text-xs mt-1 ml-1">Nome da Marca é obrigatório.</p>}
                            </div>
                            <div className="md:w-1/2 mb-2 p-2">
                                <label htmlFor="country" className="block font-semibold mb-2">País/Origem</label>
                                <select
                                    id="country"
                                    className="w-full px-3 py-2 border border-gray-300 rounded-sm focus:outline-none focus:ring-2 focus:ring-red-950"
                                    {...register('country', { required: true })}
                                >
                                    <option value="">Selecione um país</option>
                                    {countries.map(country => (
                                        <option key={country} value={country}>{country}</option>
                                    ))}
                                </select>
                                {errors.country && <p className="text-red-600 text-xs mt-1 ml-1">País/Origem é obrigatório.</p>}
                            </div>
                        </div>

                        <div className="flex items-end justify-end p-2">
                            <ButtonRegistration />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default BrandRegistration;
