"use client";

import React from 'react';

const ButtonRegistration: React.FC = () => {

    return (

        <button
            type="submit"
            className="bg-primary text-white font-bold px-4 py-2 rounded-sm hover:bg-red-800 transition duration-200"
        >
            Cadastrar
        </button>
    );
};

export default ButtonRegistration;