"use client";

import React from 'react';
import Image from 'next/image';
import logoDashboard from '../../public/logoDashboard.png'
import Link from 'next/link'

const Navbar: React.FC = () => {

    return (
        <div className="h-screen w-64 bg-primary text-white text-center flex flex-col p-4">
            <Image src={logoDashboard} alt='logo genovese' className='object-cover items-center w-60 h-auto mx-auto' />
            <ul className="space-y-4">
                <li className="mb-4 p-2 hover:bg-red-900 transition duration-200 rounded-sm">
                    <Link href="/">
                        Listagem de Produtos
                    </Link>
                </li>
                <li className="mb-4 p-2 hover:bg-red-900 transition duration-200 rounded-sm">
                    <Link href="/productRegistration">
                        Cadastro de Produto
                    </Link>
                </li>
                <li className="mb-4 p-2 hover:bg-red-900 transition duration-200 rounded-sm">
                    <Link href="/brandRegistration">
                        Cadastro de Marca
                    </Link>
                </li>
                <li className="mb-4 p-2 hover:bg-red-900 transition duration-200 rounded-sm">
                    <Link href="/categoryRegistration">
                        Cadastro de Categoria
                    </Link>
                </li>
            </ul>
        </div>
    );
};

export default Navbar;



